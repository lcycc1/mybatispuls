# mybatispuls
主要是测试mybatisx 在mysql数据库里的crud相关操作。

```
一、pom的增加依赖
        <!--
        配置三： mybatis-plus-boot-starter和mysql-connector-java依赖和druid-spring-boot-starter
        -->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.4.3.4</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.46</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>1.2.16</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
```

二、application.yml文件的修改
#二：配置数据源

```
spring:
  application:
    name: mybatis-plus
  #数据库连接相关配置
  datasource:
    url: jdbc:mysql://localhost:3306/el-admin?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true&serverTimezone=GMT%2B8&useSSL=false
    username: root
    password: a123456
    #阿里巴巴的druid的mysql连接池
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.jdbc.Driver
```

三、构造类的有参和无参构造

```
    private static final long serialVersionUID = 1L;
/*定义SysDept多参数构造函数*/
    public SysDept(Long deptId,Long pid, int subCount, String name, int deptSort, Boolean enabled, String createBy, String updateBy,Date createTime, Date updateTime) {
    this.deptId=deptId;
    this.pid=pid;
    this.subCount=subCount;
    this.name=name;
    this.enabled=enabled;
    this.createBy=createBy;
    this.updateBy=updateBy;
    this.deptSort=deptSort;
    this.createTime=createTime;
    this.updateTime=updateTime;

    }
/*定义无参数构造函数*/
    public SysDept() {

    }

```

四、在@test里面用有参和无参实例类，利用mybatisx生成的server类和mapper类，增加数据库数据
 
```
 @Autowired
    private UserMapper mapper;
    @Autowired
    private UserService service;
    @Autowired
    private SysDeptService sysDeptService;
    @Test
    void contextLoads() {
    }

    // 单个插入
    @Test
     void save() {
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        int save=mapper.insert(user);
        System.out.println(save);
    }
    @Test
    void save2(){
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        boolean save=service.save(user);
        System.out.println(save);
    }
    @Test
    void save3()
    {

        //用多参数类的构造函数实现实例
        SysDept sysDept=new SysDept(null,2l,1, "信息办",1,true,"admin","admin", new Date(System.currentTimeMillis()),new Date(System.currentTimeMillis()));
        //用无参数类的构造函数实殃实例
        SysDept sysDept1=new SysDept();
        sysDept1.setDeptId(null);
        sysDept1.setPid(1l);
        sysDept1.setSubCount(6);
        sysDept1.setName("信息办2");
        sysDept1.setDeptSort(2);
        sysDept1.setEnabled(true);
        sysDept1.setCreateBy("admin");
        sysDept1.setUpdateBy("admin");
        Date date =new Date();
        date.setTime(20011107);
        sysDept1.setCreateTime(date);
        sysDept1.setUpdateTime(new Date(System.currentTimeMillis()));
        sysDeptService.save(sysDept);
        sysDeptService.save(sysDept1);

    }

```
