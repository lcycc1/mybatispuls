package com.example.demo;


import com.example.demo.domain.SysDept;
import com.example.demo.domain.User;

import com.example.demo.mapper.UserMapper;
import com.example.demo.service.SysDeptService;
import com.example.demo.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;

@SpringBootTest
class Demo5ApplicationTests {


    @Autowired
    private UserMapper mapper;
    @Autowired
    private UserService service;
    @Autowired
    private SysDeptService sysDeptService;
    @Test
    void contextLoads() {
    }

    // 单个插入
    @Test
     void save() {
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        int save=mapper.insert(user);
        System.out.println(save);
    }
    @Test
    void save2(){
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        boolean save=service.save(user);
        System.out.println(save);
    }
    @Test
    void save3()
    {

        //用多参数类的构造函数实现实例
        SysDept sysDept=new SysDept(null,2l,1, "信息办",1,true,"admin","admin", new Date(System.currentTimeMillis()),new Date(System.currentTimeMillis()));
        //用无参数类的构造函数实殃实例
        SysDept sysDept1=new SysDept();
        sysDept1.setDeptId(null);
        sysDept1.setPid(1l);
        sysDept1.setSubCount(6);
        sysDept1.setName("信息办2");
        sysDept1.setDeptSort(2);
        sysDept1.setEnabled(true);
        sysDept1.setCreateBy("admin");
        sysDept1.setUpdateBy("admin");
        Date date =new Date();
        date.setTime(20011107);
        sysDept1.setCreateTime(date);
        sysDept1.setUpdateTime(new Date(System.currentTimeMillis()));
        sysDeptService.save(sysDept);
        sysDeptService.save(sysDept1);

    }

}
