package com.example.demo.mapper;

import com.example.demo.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Administrator
* @description 针对表【user】的数据库操作Mapper
* @createDate 2024-06-13 08:39:41
* @Entity com.example.demo.domain.User
*/

public interface UserMapper extends BaseMapper<User> {

}




