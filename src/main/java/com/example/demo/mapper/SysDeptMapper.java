package com.example.demo.mapper;

import com.example.demo.domain.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【sys_dept(部门)】的数据库操作Mapper
* @createDate 2024-06-13 15:28:16
* @Entity com.example.demo.domain.SysDept
*/
public interface SysDeptMapper extends BaseMapper<SysDept> {

}




