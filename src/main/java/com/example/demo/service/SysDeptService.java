package com.example.demo.service;

import com.example.demo.domain.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【sys_dept(部门)】的数据库操作Service
* @createDate 2024-06-13 15:28:16
*/
public interface SysDeptService extends IService<SysDept> {

}
