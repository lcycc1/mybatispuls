package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.domain.SysDept;
import com.example.demo.service.SysDeptService;
import com.example.demo.mapper.SysDeptMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【sys_dept(部门)】的数据库操作Service实现
* @createDate 2024-06-13 15:28:16
*/
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept>
    implements SysDeptService{

}




