package com.example.demo.service;

import com.example.demo.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【user】的数据库操作Service
* @createDate 2024-06-13 08:39:41
*/
public interface UserService extends IService<User> {

}
