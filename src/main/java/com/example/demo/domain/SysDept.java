package com.example.demo.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 部门
 * @TableName sys_dept
 */
@TableName(value ="sys_dept")
@Data
public class SysDept implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long deptId;

    /**
     * 上级部门
     */
    private Long pid;

    /**
     * 子部门数目
     */
    private Integer subCount;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer deptSort;

    /**
     * 状态
     */
    private Boolean enabled;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
/*定义SysDept多参数构造函数*/
    public SysDept(Long deptId,Long pid, int subCount, String name, int deptSort, Boolean enabled, String createBy, String updateBy,Date createTime, Date updateTime) {
    this.deptId=deptId;
    this.pid=pid;
    this.subCount=subCount;
    this.name=name;
    this.enabled=enabled;
    this.createBy=createBy;
    this.updateBy=updateBy;
    this.deptSort=deptSort;
    this.createTime=createTime;
    this.updateTime=updateTime;

    }
/*定义无参数构造函数*/
    public SysDept() {

    }
}