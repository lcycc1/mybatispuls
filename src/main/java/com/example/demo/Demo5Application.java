package com.example.demo;



import com.example.demo.mapper.UserMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/*
配置一：
DataSourceAutoConfiguration 关闭和配置ComponentScan
*/
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@MapperScan("com.example.demo.mapper")

public class Demo5Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo5Application.class, args);
    }

}
