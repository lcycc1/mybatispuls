create table sys_dept
(
    dept_id     bigint auto_increment comment 'ID'
        primary key,
    pid         bigint          null comment '上级部门',
    sub_count   int default 0   null comment '子部门数目',
    name        varchar(255)    not null comment '名称',
    dept_sort   int default 999 null comment '排序',
    enabled     bit             not null comment '状态',
    create_by   varchar(255)    null comment '创建者',
    update_by   varchar(255)    null comment '更新者',
    create_time datetime        null comment '创建日期',
    update_time datetime        null comment '更新时间'
)
    comment '部门';

create index inx_enabled
    on sys_dept (enabled);

create index inx_pid
    on sys_dept (pid);

create table sys_menu
(
    menu_id     bigint auto_increment comment 'ID'
        primary key,
    pid         bigint           null comment '上级菜单ID',
    sub_count   int default 0    null comment '子菜单数目',
    type        int              null comment '菜单类型',
    title       varchar(100)     null comment '菜单标题',
    name        varchar(100)     null comment '组件名称',
    component   varchar(255)     null comment '组件',
    menu_sort   int              null comment '排序',
    icon        varchar(255)     null comment '图标',
    path        varchar(255)     null comment '链接地址',
    i_frame     bit              null comment '是否外链',
    cache       bit default b'0' null comment '缓存',
    hidden      bit default b'0' null comment '隐藏',
    permission  varchar(255)     null comment '权限',
    create_by   varchar(255)     null comment '创建者',
    update_by   varchar(255)     null comment '更新者',
    create_time datetime         null comment '创建日期',
    update_time datetime         null comment '更新时间',
    constraint uniq_name
        unique (name),
    constraint uniq_title
        unique (title)
)
    comment '系统菜单';

create index inx_pid
    on sys_menu (pid);

create table sys_role
(
    role_id     bigint auto_increment comment 'ID'
        primary key,
    name        varchar(100) not null comment '名称',
    level       int          null comment '角色级别',
    description varchar(255) null comment '描述',
    data_scope  varchar(255) null comment '数据权限',
    create_by   varchar(255) null comment '创建者',
    update_by   varchar(255) null comment '更新者',
    create_time datetime     null comment '创建日期',
    update_time datetime     null comment '更新时间',
    constraint uniq_name
        unique (name)
)
    comment '角色表';

create index role_name_index
    on sys_role (name);

create table sys_roles_depts
(
    role_id bigint not null,
    dept_id bigint not null,
    primary key (role_id, dept_id)
)
    comment '角色部门关联';

create index FK7qg6itn5ajdoa9h9o78v9ksur
    on sys_roles_depts (dept_id);

create table sys_roles_menus
(
    menu_id bigint not null comment '菜单ID',
    role_id bigint not null comment '角色ID',
    primary key (menu_id, role_id)
)
    comment '角色菜单关联';

create index FKcngg2qadojhi3a651a5adkvbq
    on sys_roles_menus (role_id);

create table sys_user
(
    user_id        bigint auto_increment comment 'ID'
        primary key,
    dept_id        bigint           null comment '部门名称',
    username       varchar(180)     null comment '用户名',
    nick_name      varchar(255)     null comment '昵称',
    gender         varchar(2)       null comment '性别',
    phone          varchar(255)     null comment '手机号码',
    email          varchar(180)     null comment '邮箱',
    avatar_name    varchar(255)     null comment '头像地址',
    avatar_path    varchar(255)     null comment '头像真实路径',
    password       varchar(255)     null comment '密码',
    is_admin       bit default b'0' null comment '是否为admin账号',
    enabled        bit              null comment '状态：1启用、0禁用',
    create_by      varchar(255)     null comment '创建者',
    update_by      varchar(255)     null comment '更新者',
    pwd_reset_time datetime         null comment '修改密码的时间',
    create_time    datetime         null comment '创建日期',
    update_time    datetime         null comment '更新时间',
    constraint UK_kpubos9gc2cvtkb0thktkbkes
        unique (email),
    constraint uniq_email
        unique (email),
    constraint uniq_username
        unique (username),
    constraint username
        unique (username)
)
    comment '系统用户';

create index FK5rwmryny6jthaaxkogownknqp
    on sys_user (dept_id);

create index inx_enabled
    on sys_user (enabled);

create table sys_users_roles
(
    user_id bigint not null comment '用户ID',
    role_id bigint not null comment '角色ID',
    primary key (user_id, role_id)
)
    comment '用户角色关联';

create index FKq4eq273l04bpu4efj0jd0jb98
    on sys_users_roles (role_id);

create table user
(
    userid int auto_increment
        primary key,
    name   char(50) charset utf8mb3 null,
    age    int                      null
);

